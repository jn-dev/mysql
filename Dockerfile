FROM alpine:latest

COPY ./entrypoint.sh /

RUN apk update && apk add tzdata

RUN addgroup -S mysql \
    && adduser -S mysql -G mysql \
    && apk add --no-cache mysql mysql-client \
    && rm -f /var/cache/apk/* \
    && awk '{ print } $1 ~ /\[mysqld\]/ && c == 0 { c = 1; print "skip-host-cache\nskip-name-resolve\nlower_case_table_names=1"}' /etc/mysql/my.cnf > /tmp/my.cnf \
    && mv /tmp/my.cnf /etc/mysql/my.cnf \
    && mkdir /run/mysqld \
    && chown -R mysql:mysql /run/mysqld \
    && chmod -R 777 /run/mysqld

#Nastaveni timezone
RUN cp /usr/share/zoneinfo/Europe/Prague /etc/localtime

EXPOSE 3306

ENV DB_ROOT_PASSWORD root

ENTRYPOINT ["/entrypoint.sh"]

CMD ["mysqld", "--user=mysql"]
